<?php
/**
 * Template Name: Over ons Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <div class='full parallax' style='background-image: url(<?php the_field('field_56d85057b5adb'); ?>); color: #fff;'>
        <div class='row'>
          <div class='twelve columns'>
            <div class='big mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 class="text-shadow-pls" style='color: #fff'>
                  <?php the_title(); ?>
                </h2>
              </div>
              <h3 class='centered-text text-shadow-pls' style='color: #fff'><?php the_field('field_56d82fa6d8ed2'); ?></h3>
            </div>
          </div>
        </div>
        <div class='four spacing'></div>
      </div>
      <div class='full'>
        <div class='row'>
          <div class='medium-6 columns'>
            <?php the_content(); ?>
          </div>
          <div class='medium-6 columns'>
            <?php the_field('field_56d82fe6dc807'); ?>
          </div>
        </div>
      </div>
      <div class="three spacing"></div>
       <?php if(get_field('field_56fa3d315824e')): ?>
        <div class='full parallax' style='background-image: url(<?php the_field('field_56fa3d315824e');?>); height: 300px;'></div>
       <?php endif; ?>
      <?php if(get_field('field_56d830dbbfa0f')): ?>
      <div class='full parallax' style='background: #f5f5f5'>
        <div class='row'>
          <div class='large-12 columns'>
            <p class='centered-text'>
              <i class='fa fa-quote-left' style='color: #5b9036; font-size: 36px;'></i>
            </p>
          </div>
        </div>
        <div class='row'>
          <div class='large-12 columns'>
            <div class='mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 style=''>
                  <?php the_field('field_56d830dbbfa0f'); ?>
                </h2>
              </div>
            </div>
            <div class='spacing'></div>
          </div>
        </div>
      </div>
    <?php else: ?>
     <div class='spacing'></div>
    <?php endif; ?>
<?php endwhile; ?>
