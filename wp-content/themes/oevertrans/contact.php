<?php
/**
 * Template Name: Contact Template
 */
$location = get_field('field_56d7f1b7fdc37');
?>



<?php while (have_posts()) : the_post(); ?>
  <div class='full parallax' style='background-image: url(<?php the_field('field_56d851d6d5f96'); ?>); color: #fff;'>
        <div class='row'>
          <div class='large-12 columns'>
            <div class='big mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 class="text-shadow-pls" style='color: #fff'>
                  <?php the_title() ?>
                </h2>
              </div>
              <h3 class='centered-text text-shadow-pls' style='color: #fff'><?php the_field('field_56d801dcf2b55'); ?></h3>
            </div>
          </div>
        </div>
        <div class='four spacing'></div>
      </div>
      <div class='full'>
        <div class='row'>
          <div class='medium-8 columns'>
            <div class='form'>
              <div class='row'>
                  <div class='medium-12 columns'>
                    <?php gravity_form(1, false, false, false, null, true, null, true ); ?>
                  </div>
              </div>
            </div>
            <div class='two spacing'></div>
          </div>
          <div class='medium-4 columns'>
            <div class='contact-details'>
              <?php if(get_field('field_56d6ea935e8e1')): ?>
                  <h4>Adres:</h4>
                  <p>
                  <?php the_field('field_56d6ea935e8e1') ?>
                  <?php if(get_field('field_56fe7551c6b21')): ?>
                      <a class="route" href="<?php the_field('field_56fe7551c6b21') ?>" target="_blank">Routebeschrijving</a>
                  <?php endif; ?>
                  </p>
                  
              <?php endif; ?>
              
              
              <?php if(get_field('field_56d6eab65e8e2')): ?>
                  <div class="row">
                      <div class="col-md-12" style="padding-left: 15px;">
                          <h4 class="pull-left" style="padding-right: 5px;">Telefoon:</h4>
                          <span><?php the_field("field_56d6eab65e8e2"); ?></span>
                      </div>
                  </div>
              <?php endif; ?>
              
              <?php if(get_field('field_56d6eac95e8e3')): ?>
                  <div class="row">
                      <div class="col-md-12" style="padding-left: 15px;">
                          <h4 class="pull-left" style="padding-right: 5px;">Fax:</h4>
                          <span><?php the_field("field_56d6eac95e8e3"); ?></span>
                      </div>
                  </div>
              <?php endif; ?>
              
              <?php if(get_field('field_56d6ead15e8e4')): ?>
                  <div class="row">
                      <div class="col-md-12" style="padding-left: 15px;">
                          <h4 class="pull-left clearfix" style="padding-right: 5px;">E-mail</h4>
                          <span><a href="mailto:<?php the_field("field_56d6ead15e8e4"); ?>"><?php the_field("field_56d6ead15e8e4"); ?></a></span>
                      </div>
                  </div>
              <?php endif; ?>
              
              <?php if(get_field('field_56d6ead85e8e5')): ?>
                  <div class="row">
                      <div class="col-md-12" style="padding-left: 15px;">
                          <h4 class="pull-left" style="padding-right: 5px;">BTW:</h4>
                          <span><?php the_field("field_56d6ead85e8e5"); ?></span><br /><br />
                      </div>
                  </div>
              <?php endif; ?>
              
              <?php if(get_field('field_56d6eae05e8e6')): ?>
                  <h4>Openingsuren:</h4>
                  <p><?php the_field("field_56d6eae05e8e6"); ?></p>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class='four spacing'></div>
    <div id="map" style="width: 100%; height: 400px;"></div>
<?php endwhile; ?>
    <script>
        function initMap() {var styles = [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#302370"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]
            
            var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});
            
            var myLatLng = {lat: <?php echo $location['lat']; ?>, lng: <?php echo $location['lng']; ?>};
            
             var contentString = '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
              '<h3 id="firstHeading" class="firstHeading">Oevertrans</h3>'+
              '<div id="bodyContent">'+
              '<p><?php echo $location['address']; ?></p>'+
              '</div>'+
              '</div>';
            
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: myLatLng,
                scrollwheel: false,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                }
            });
            
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Oevertrans'
            });
            
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        };
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyCWVbhuhALr75Gf1axq1G_XO79QjCq80no"
        async defer></script>