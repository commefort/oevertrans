<?php
/**
 * Template Name: Homepagina Template
 */
$carouselimages = get_field('field_56d84a82f41c2');
$images = get_field('field_56d844d894461');
$dienstenimages = get_field('field_56d84d78b7afe');
?>

<?php while (have_posts()) : the_post(); ?>
    <div class='mod modDefaultSlider'>
        <div class='sequence' data-autostop='on' data-timeout='0'>
          <a class='sequence-prev' href='javascript:void(0);'>
            <span></span>
          </a>
          <a class='sequence-next' href='javascript:void(0);'>
            <span></span>
          </a>
          
          <?php if( $carouselimages ): ?> 
          <ul class='sequence-pagination'>
           <?php foreach( $carouselimages as $image ): ?>
            <li></li>
            <?php endforeach; ?>
          </ul>
          <?php endif; ?>
          
          <?php if( $carouselimages ): ?> 
          <ul class='sequence-canvas'>
           <?php foreach( $carouselimages as $image ): ?>
            <li class='frame  centered-text'>
              <div class='bg' style='background-image: url(<?php echo $image['url']; ?>);'></div>
              <div class='small-title bottom-to-top'>
                <div class='row'>
                  <div class='large-12 columns'>
                    <h2 class="text-shadow-pls"><?php echo $image['caption']; ?></h2>
                  </div>
                </div>
              </div>
              <div class='title bottom-to-top'>
                <div class='row'>
                  <div class='large-12 columns'>
                    <h2 class="text-shadow-pls"><?php echo $image['title']; ?></h2>
                  </div>
                </div>
              </div>
              <?php if( get_field('link', $image['id']) ): ?>
              <div class='buttons-wrapper bottom-to-top'>
                <div class='row'>
                  <div class='large-12 columns'>
                    <div class='two spacing'></div>
                    <a class='button' href='<?php the_field('link', $image['id']); ?>'>Lees Meer</a>
                  </div>
                </div>
              </div>
              <?php endif; ?>
            </li>
           <?php endforeach; ?>
          </ul>
         <?php endif; ?>
        </div>
      </div>
      <!-- end carousel -->
      <div class="full">
         <div class='row'>
          <div class='large-12 columns'>
            <div class='mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 style=''><?php the_field('field_56d83ebc19720'); ?></h2>
              </div>
            </div>
            <div class='spacing'></div>
            <p><?php the_content(); ?></p>
          </div>
        </div>
         <div class="three spacing"></div> 
         <div class='mod modGallery'>
         <?php if( $images ): ?>
          <ul class='gallery large-block-grid-4 medium-block-grid-3 small-block-grid-2'>
             <?php foreach( $images as $image ): ?>
              <li>
               <a style="cursor: default;">
               <img width="400" height="400" alt="" src="<?php echo $image['url']; ?>" />
                <div class='overlay' style='background: rgba(143, 221, 115, 0.8);'>
                  <div class='thumb-info'>
                    <h3><?php echo $image['title']; ?></h3>
                    <p><?php echo $image['caption']; ?></p>
                  </div>
                </div>
                </a>
             </li>
            <?php endforeach; ?>
          </ul>
         <?php endif; ?>
        </div> 
      </div>
      <!-- end afbeeldingen rooster -->
      <div class='full' #diensten>
        <div class='row'>
          <div class='large-12 columns'>
            <div class='mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 style=''><?php the_field('field_56d8427ffd1d7'); ?></h2>
              </div>
            </div>
            <div class='spacing'></div>
            <p><?php the_field('field_56d8428efd1d8'); ?></p>
            <div class='three spacing'></div>
          </div>
        </div>
        <div class='row'>
          <div class='medium-6 columns'>
           <?php if( $dienstenimages ): ?>
            <?php foreach( $dienstenimages as $image ): ?>
            <img class="fadeinleft" width="500" height="900" alt="" src="<?php echo $image['url']; ?>" />
            <div class='two spacing'></div>
            <?php endforeach; ?>
           <?php endif; ?>
          </div>
          <div class='medium-6 columns'>
            <div class='fadein mod modIconText' data-delay='200'>
                <?php the_field('field_56d84e7f03c5d'); ?>
            </div>
          </div>
        </div>
      </div>
      <!-- end diensten -->
      <div class='full no-padding' style='background: #fff'>
        <div class='two spacing'></div>
        <div class='mod modCallToAction'>
          <div class='row'>
            <div class='medium-9 large-9 columns'>
              <p><?php the_field('field_56d84f89648c5'); ?></p>
            </div>
          </div>
        </div>
        <div class='spacing'></div>
      </div>
<?php endwhile; ?>
