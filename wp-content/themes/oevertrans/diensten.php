<?php
/**
 * Template Name: Diensten Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <div class='full parallax' style='background-image: url(<?php the_field('field_56d85109e9b04'); ?>); color: #fff;'>
        <div class='row'>
          <div class='large-12 columns'>
            <div class='big mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 class="text-shadow-pls" style='color: #fff'>
                  <?php the_title(); ?>
                </h2>
              </div>
              <h3 class='centered-text text-shadow-pls' style='color: #fff'><?php the_field('field_56d804ca2585b'); ?></h3>
            </div>
          </div>
        </div>
        <div class='four spacing'></div>
    </div>
    <div class="full">
    <?php if( have_rows('inhoud') ): ?>
        <?php while ( have_rows('inhoud') ) : the_row(); ?>
            <?php if( get_row_layout() == 'afbeelding_links' ): ?>
                <div class="row">
                    <div class='medium-6 columns'>
                        <img class="fadeinleft" width="500" height="450" alt="" src="<?php the_sub_field('afbeelding'); ?>" />
                        <div class='two spacing'></div>
                    </div>
                    <div class='medium-6 columns'>
                        <div class='fadeinleft' data-delay='0'>
                            <?php the_sub_field('tekst'); ?>
                            <div class='two spacing'></div>
                        </div>
                    </div>
                </div>
                <div class='two spacing'></div>
            <?php elseif( get_row_layout() == 'afbeelding_rechts' ): ?>
                <div class="row">
                    <div class="medium-6 columns">
                        <div class='fadeinright' data-delay='300'>
                            <?php the_sub_field('tekst'); ?>
                            <div class='two spacing'></div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <img class="fadeinright" width="500" height="450" alt="" src="<?php the_sub_field('afbeelding'); ?>" />
                        <div class='two spacing'></div>
                    </div>
                </div>
                <div class='two spacing'></div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
<?php endwhile; ?>
