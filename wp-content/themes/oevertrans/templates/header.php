<nav class='top-bar' data-options='sticky_on: large' data-topbar=''>
        <ul class='title-area'>
          <li class='name'>
            <h1>
              <a href='<?php bloginfo('url'); ?>'>
                <img alt="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" />
              </a>
            </h1>
          </li>
          <li class='toggle-topbar menu-icon'>
            <a href='#'>Menu</a>
          </li>
        </ul>
        <section class='top-bar-section'>
          <?php wp_nav_menu( array( 'menu_class' => 'right' ) ); ?>
        </section>
</nav>
