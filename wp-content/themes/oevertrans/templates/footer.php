<div id="footer">
    <div class='three spacing'></div>
      <div class='row'>
        <div class='large-3 medium-3 columns'>
          <h1>
            <a href='<?php bloginfo('url'); ?>'>
              <img width="" height="" alt="logo white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-footer.png" />
            </a>
          </h1>
          <p>©<?php echo date('Y'); ?> Oevertrans.</p>
          <div class='spacing'></div>
          <ul class='socials'>
            <li>
              <a href='<?php the_field('linkedin', 'option'); ?>'>
                <i class='fa fa-linkedin'></i>
              </a>
            </li>
          </ul>
          <div class='spacing'></div>
        </div>
        <div class='large-3 medium-3 columns'>
          <div class='spacing'></div>
          <div class='links'>
            <h4>Sitemap</h4>
            <?php wp_nav_menu( array() ); ?>
          </div>
          <div class='spacing'></div>
        </div>
      </div>
      <div class='two spacing'></div>
</div>