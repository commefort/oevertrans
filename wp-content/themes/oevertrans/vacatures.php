<?php
/**
 * Template Name: Vacatures Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class='full parallax' style='background-image: url(<?php the_field('field_56d851743edeb'); ?>); color: #fff;'>
        <div class='row'>
          <div class='large-12 columns'>
            <div class='big mod modSectionHeader'>
              <div class='special-title centered-text'>
                <h2 class="text-shadow-pls" style='color: #fff'>
                  <?php the_title(); ?>
                </h2>
              </div>
              <h3 class='centered-text text-shadow-pls' style='color: #fff'><?php the_field('field_56d80278ab927'); ?></h3>
            </div>
          </div>
        </div>
        <div class='four spacing'></div>
      </div>
      <div class='full'>
        <div class='row'>
          <div class='medium-8 columns'>
            <div class='form'>
                <?php the_content(); ?>
            </div>
            <div class='two spacing'></div>
          </div>
          <div class='medium-4 columns'>
            <h3>Hoe kan u ons contacteren?</h3>
            <h4>E-mail:</h4>
              <p><a href="mailto:<?php the_field("field_56d6ed6aa824b"); ?>"><?php the_field('field_56d6ed6aa824b'); ?></a></p>
            <h4>Telefoon:</h4>
            <p><?php the_field('field_56d6ed72a824c'); ?></p>
          </div>
        </div>
      </div>
      <div class='four spacing'></div>
  
<?php endwhile; ?>
